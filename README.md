# Accutics Backend Developer – Code challenge


## Project overview
- Implement an endpoint that returns a list of campaigns
- Implement an endpoint for listing users.
- Implement an endpoint for creating a campaign.

## List of API endpoint
User endpoint
http://localhost:8080/api/v1/users

Campaigns endpoint
http://localhost:8080/api/v1/campaigns

Get user by Email
http://localhost:8080/api/v1/users/mpspmp2003@gmail.com

Get campaign with the pagination
http://localhost:8080/api/v1/campaigns/campaignspage/

Get campaign with specific page
http://localhost:8080/api/v1/campaigns/campaignspage?page=1


## Pre-requisites
Intellij IDE

## Technologies
If you want to run this project you need Spring Data JPA with spring boot,PostgreSQL as dependency.In this project i have make jar file using https://start.spring.io/

