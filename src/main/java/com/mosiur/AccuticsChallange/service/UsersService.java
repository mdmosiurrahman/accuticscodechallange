package com.mosiur.AccuticsChallange.service;
import com.mosiur.AccuticsChallange.models.Users;


public interface UsersService  {

    Users getUsersByEmail(String email);
}
