package com.mosiur.AccuticsChallange.service;

import com.mosiur.AccuticsChallange.models.Users;
import com.mosiur.AccuticsChallange.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class ImplementationUsersService implements UsersService {
    @Autowired
    private UsersRepository usersRepository;

    @Override
    public Users getUsersByEmail(String email){
        return usersRepository.findAll()
                .stream()
                .filter((users)-> users.getEmail().equals(email))
                .findAny()
                .orElse(new Users());
    }
}
