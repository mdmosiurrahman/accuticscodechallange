package com.mosiur.AccuticsChallange.DataBaseSeeder;

import com.mosiur.AccuticsChallange.models.Campaigns;
import com.mosiur.AccuticsChallange.models.Inputs;
import com.mosiur.AccuticsChallange.models.Users;
import com.mosiur.AccuticsChallange.repositories.CampaignsRepository;
import com.mosiur.AccuticsChallange.repositories.InputsRepository;
import com.mosiur.AccuticsChallange.repositories.UsersRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Component;


/**
 * Seeder component that adds data to PostgreSQL if it is empty. If you want an empty one change the boolean attribute
 * emptyDatabase to true.
 */
@Component
public class DataSeeder {

    @Autowired
    private DataSeeder dataSeeder;

    @Autowired
    private CampaignsRepository campaignsRepository;

    private boolean emptyDatabase = false;

    @Autowired
    private InputsRepository inputsRepository;

    @Autowired
    private UsersRepository usersRepository;

    @EventListener
    public void seedDatabase(ContextRefreshedEvent event){


        if(campaignsRepository.findAll().isEmpty() && !emptyDatabase){
            seedCampaigns();
        }
        if(inputsRepository.findAll().isEmpty() && !emptyDatabase){
            seedInputs();
        }
        if(usersRepository.findAll().isEmpty() && !emptyDatabase){
            seedUsers();
        }

    }

    private void seedUsers() {
        Users newUsersOne = new Users();
        newUsersOne.setName("Mosiur Rahman");
        newUsersOne.setEmail("mpspmp2003@gmail.com");
        usersRepository.save(newUsersOne);

        Users newUsersTwo = new Users();
        newUsersTwo.setName("Sohel Rana");
        newUsersTwo.setEmail("abc2003@gmail.com");
        usersRepository.save(newUsersTwo);

        Users newUsersThree = new Users();
        newUsersThree.setName("Amelia Rahman");
        newUsersThree.setEmail("amelia@gmail.com");
        usersRepository.save(newUsersThree);
    }

    private void seedInputs() {
        Inputs newInputsOne = new Inputs();
        newInputsOne.setType("channel");
        newInputsOne.setValue("TV2");
        inputsRepository.save(newInputsOne);

        Inputs newInputsTwo = new Inputs();
        newInputsTwo.setType("source");
        newInputsTwo.setValue("sattelite");
        inputsRepository.save(newInputsTwo);
    }

    private void seedCampaigns() {
        Campaigns newCampaignsOne = new Campaigns();
        campaignsRepository.save(newCampaignsOne);
        Campaigns newCampaignsTwo = new Campaigns();
        campaignsRepository.save(newCampaignsTwo);
    }


}
