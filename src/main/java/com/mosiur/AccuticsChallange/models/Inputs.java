package com.mosiur.AccuticsChallange.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;

@Entity
public class Inputs {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int inputid;
    private String type;
    private String value;

    @ManyToOne
    @JoinColumn(name = "campaign_id")
    private Campaigns campaigns;

    @JsonGetter("")
    public String campaigns() {
        if (campaigns != null) {
            return "/api/v1/campaigns/" + campaigns.getCampaignId();
        } else {
            return null;
        }
    }

    public int getInputid() {
        return inputid;
    }

    public void setInputid(int inputid) {
        this.inputid = inputid;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
