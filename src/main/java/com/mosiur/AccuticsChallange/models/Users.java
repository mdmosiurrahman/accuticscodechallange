package com.mosiur.AccuticsChallange.models;


import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Users {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int UserId;

    private String name;
    private String email;

    @OneToMany(mappedBy = "users")
    List<Campaigns> campaigns;

    @JsonGetter("campaigns")
    public List<String> movies() {
        if (campaigns != null) {
            return campaigns.stream()
                    .map(campaign -> {
                        return "/api/v1/campaigns/" + campaign.getCampaignId();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public int getUserId() {
        return UserId;
    }

    public void setUserId(int userId) {
        UserId = userId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
}
