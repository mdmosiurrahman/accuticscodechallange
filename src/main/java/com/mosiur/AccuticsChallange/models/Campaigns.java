package com.mosiur.AccuticsChallange.models;

import com.fasterxml.jackson.annotation.JsonGetter;

import javax.persistence.*;
import java.util.List;
import java.util.stream.Collectors;

@Entity
public class Campaigns {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int campaignId;


    @ManyToOne
    @JoinColumn(name = "user_id")
    private Users users;


    @JsonGetter("")
    public String users() {
        if (users != null) {
            return "/api/v1/users/" + users.getUserId();
        } else {
            return null;
        }
    }

    @OneToMany(mappedBy = "campaigns")
    List<Inputs> inputs;

    @JsonGetter("inputs")
    public List<String> inputs() {
        if (inputs != null) {
            return inputs.stream()
                    .map(input -> {
                        return "/api/v1/inputs/" + input.getInputid();
                    }).collect(Collectors.toList());
        }
        return null;
    }

    public int getCampaignId() {
        return campaignId;
    }

    public void setCampaignId(int campaignId) {
        this.campaignId = campaignId;
    }
}
