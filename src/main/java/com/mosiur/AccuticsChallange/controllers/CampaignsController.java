package com.mosiur.AccuticsChallange.controllers;


import com.mosiur.AccuticsChallange.models.Campaigns;
import com.mosiur.AccuticsChallange.repositories.CampaignsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Optional;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/campaigns")
public class CampaignsController {

    @Autowired
    private CampaignsRepository campaignsRepository;

    /**
     * Fetching campaigns from the database.
     * @param page: Page number
     * @param sortBy: Sort by attributes
     * @return : Returns a List of characters.
     */
    @GetMapping("/campaignspage")
    Page<Campaigns> getAllCampaigns(
            @RequestParam Optional<Integer> page
            ,@RequestParam Optional<String> sortBy
    ){
        return campaignsRepository.findAll(
                PageRequest.of(
                        page.orElse(0),
                        3
                        , Sort.Direction.ASC, sortBy.orElse("campaignId")
                )
        );
    }

    /**
     * Fetching all the current campaigns from the database.
     * @return : Returns a List of campaigns.
     */
    @GetMapping()
    public ResponseEntity<List<Campaigns>> getAllCampaigns() {
        List<Campaigns> campaings = campaignsRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(campaings, resp);
    }

    /**
     * Adding a campaigns to the database
     * @param campaigns : Campaigns.
     * @return : New campaign object.
     */
    @PostMapping
    public ResponseEntity<Campaigns> addCampaigns(@RequestBody Campaigns campaigns) {
        Campaigns returnCampaigns = campaignsRepository.save(campaigns);
        HttpStatus status = HttpStatus.CREATED;
        return new ResponseEntity<>(returnCampaigns, status);
    }
}
