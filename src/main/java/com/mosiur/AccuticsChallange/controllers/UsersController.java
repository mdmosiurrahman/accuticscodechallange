package com.mosiur.AccuticsChallange.controllers;
import com.mosiur.AccuticsChallange.models.Users;
import com.mosiur.AccuticsChallange.repositories.UsersRepository;
import com.mosiur.AccuticsChallange.service.UsersService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;

import org.springframework.web.bind.annotation.*;


import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/api/v1/users")
public class UsersController {

    @Autowired
    private UsersRepository usersRepository;
    @Autowired
    private UsersService usersService;

    /**
     * Fetching all the current users from the database.
     * @return : Returns a List of users.
     */
    @GetMapping()
    public ResponseEntity <List<Users>> getAllUsers() {
        List<Users> users = usersRepository.findAll();
        HttpStatus resp = HttpStatus.OK;
        return new ResponseEntity<>(users, resp);

    }

    /**
     * Fetch a users from the database with a given email.
     * @param email: email identifier.
     * @return : Returns the users from given email.
     */
    @GetMapping("/{email}")
    public ResponseEntity<Users> getUsersByEmail(@PathVariable("email") String email){
        Users user = usersService.getUsersByEmail(email);
        HttpHeaders header=new HttpHeaders();
        header.add("desc", "Getting users by email");
        return ResponseEntity.status(HttpStatus.OK).headers(header).body(user);
    }
}
