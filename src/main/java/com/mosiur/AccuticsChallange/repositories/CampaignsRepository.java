package com.mosiur.AccuticsChallange.repositories;

import com.mosiur.AccuticsChallange.models.Campaigns;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CampaignsRepository extends JpaRepository<Campaigns,Integer> {
}
