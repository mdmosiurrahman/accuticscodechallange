package com.mosiur.AccuticsChallange.repositories;

import com.mosiur.AccuticsChallange.models.Users;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;
import java.util.Map;

public interface UsersRepository extends JpaRepository<Users,Integer> {

    }
