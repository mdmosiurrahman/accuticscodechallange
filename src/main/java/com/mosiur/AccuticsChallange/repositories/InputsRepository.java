package com.mosiur.AccuticsChallange.repositories;

import com.mosiur.AccuticsChallange.models.Inputs;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface InputsRepository extends JpaRepository<Inputs,Integer> {
}
