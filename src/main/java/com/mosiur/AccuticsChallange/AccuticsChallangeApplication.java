package com.mosiur.AccuticsChallange;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class AccuticsChallangeApplication {

	public static void main(String[] args) {
		SpringApplication.run(AccuticsChallangeApplication.class, args);
	}

}
